let obj = {
  name: 'Jane',
  age: 23,
  hobby: {
    hobbyName1: 'programming',
    hobbyName2: 'embroidery',
    hobbyName3: [12, 34, 67]
  }
};

function cloningObj(obj) {
  let cloneObj = {};
  for (let key in obj) {
    if (typeof (obj[key]) == 'object') {
      cloneObj[key] = cloningObj(obj[key]);
    }
    else {
      cloneObj[key] = obj[key];
    }  
  }
  return cloneObj;
}
let newObj = cloningObj(obj)
console.log(newObj);
