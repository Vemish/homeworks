let btn = document.getElementById('btn');
let body = document.getElementById('body');
btn.addEventListener('click', function(event) {
	btn.classList.toggle('light-theme-btn');
	body.classList.toggle('blue');
	if (btn.classList.contains('light-theme-btn')) {
		btn.innerText = 'Light theme';
		localStorage.setItem('btnTheme', 'light');
		localStorage.setItem('bodyTheme', 'blue');
	} else {
		btn.innerText = 'Blue theme';
		localStorage.setItem('btnTheme', 'blue');
		localStorage.setItem('bodyTheme', 'light');
	};

	
});

window.onload = function (event) {
	if (localStorage.getItem('btnTheme') == 'light' && localStorage.getItem('bodyTheme') == 'blue') {
		btn.classList.add('light-theme-btn');
		body.classList.add('blue');
		btn.innerText = 'Light theme';
	} else {
		btn.innerText = 'Blue theme';
		return false;
	}
};
