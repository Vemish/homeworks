let button = document.getElementById('button');

button.addEventListener('click', function () {
	this.remove();
	addInputs();
});

function addInputs(argument) {
	let circleDiameter = document.createElement('input');
	circleDiameter.setAttribute('type', 'text');
	circleDiameter.setAttribute('placeholder', 'Введите диаметр');
	let circleColour = document.createElement('input');
	circleColour.setAttribute('type', 'text');
	circleColour.setAttribute('placeholder', 'Введите цвет круга');
	let paint = document.createElement('button');
	paint.innerHTML = 'Нарисовать';
	paint.id = "paint";
	document.body.appendChild(circleDiameter);
	document.body.appendChild(circleColour);
	document.body.appendChild(paint);

	paint.addEventListener('click', function () {
		let diameter = circleDiameter.value;
		let colour = circleColour.value;
		addCircle(diameter, colour);
	});
};

function addCircle(circleDiameter, circleColour) {
	let circle = document.createElement('div');
	circle.style.width = circleDiameter+'px';
	circle.style.height = circleDiameter+'px';
	circle.style.background = circleColour;
	document.body.appendChild(circle);
}