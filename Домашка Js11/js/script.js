let list_length = prompt('Сколько пунктов в списке?', '1');
let elems = [];

for (let i = 1; i <= list_length; i++) {
  let elem = prompt('Текст пункта', '');
  elems.push(elem);
}

let list = document.createElement('ul');
document.body.appendChild(list);

elems.map( (value) => {
  let listItem = document.createElement('li');
  listItem.innerHTML = value;
  list.appendChild(listItem);
});

let timerEl = document.createElement('span');
timerEl.id = 'timer';
document.body.appendChild(timerEl);

let j = 11;
let interval = setInterval( () => {
  if( j == 1 ) {
    document.body.innerHTML = '';
    clearInterval(interval);
  } else {
    j--;
    timerEl.innerHTML = j;
  }
}, 1000);